import math
import numpy as np
import arrs as arp
from mpi4py import MPI
"""
=====================================================================================================
                                    AUTOENCODER METHOD
=====================================================================================================
"""

def sigmoid_function(z):
    """
    fungsi sigmoid logistik 1 / 1 + exp(-x)
    :z merupakan kalkulasi network antara w,b dengan formulasi
    sigma W.x + b ; dimana x adalah nilai input dari input layer
    """
    return (1/(1+ math.exp(-z)))

def weights_transpose(weight):

    weight_transpose = []
    for l in range(len(weight)-1,-1, -1):
        W = weight[l]
        weight_transpose.append( W.T )
    return weight_transpose

def summing_network(Weight, Input, Bias, N_Input):

    sum = 0.0
    for j in range(0, N_Input + 1):
        if j == N_Input :
            sum += Bias
        else:            
            sum += Weight[j] * Input[j]
    return sum

def activation_calculation(Weight, Input, Bias, N_Input, N_Hidden, COMM, RANK, SIZE):
    """
    #   Bagi total n_hidden berdasarkan jumlah matrix yang sudah di slice
    #   setelah itu running looping sebanyak matrix yang telah di pecah
    #   ambil jumlah N dengan len(data)
    """

    Network = np.zeros((N_Hidden))
    for r in range(SIZE):
        if RANK == r :
            for i in range(0, N_Hidden):
                Network[i] = sigmoid_function( summing_network(Weight[i], Input, Bias[i], N_Input) )
            COMM.Barrier()
    return Network

"""
=====================================================================================================
                            AUTOENCODER ERROR METHOD
=====================================================================================================
"""
def cost_function(input_x,hypothesis_x):
    """
    cost function merupakan fungsi biaya yang mengkalkulasikan
    antara output target dengan output aktual dimana output actual adalah y
    dan output target dalah fungsi hypothesis
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    :y merupakan output aktual
    """
    return 0.5 * math.pow(abs(hypothesis_x - input_x ) , 2)

def compute_cost_function(output, y_target,  COMM, RANK, SIZE):

    error_cf = []
    for r in range(SIZE):
        if RANK == r :
            for i in range(0, len(output)):
                error_cf.append(cost_function(output[i], y_target[i]))
            COMM.Barrier()
    return error_cf

def inner_weight_decay(weights, COMM, RANK, SIZE):

    sum = 0.0
    for r in range(SIZE):
        if RANK == r :
            for i in range(0, len(weights[0])):
                for j in range(0, len(weights)):
                    sum += math.pow(weights[j][i], 2)
            COMM.Barrier()
    return sum

def sparsity_constrain( recontruction_x, hidden_decoder, hidden_encoder, layer_size):

    sparsity = np.zeros((layer_size,1))
    sum = 0

    for l in range(0,layer_size):
        if l == layer_size :
            for i in range(0,len(recontruction_x)):
                sum += recontruction_x[i]
            sparsity[l] = sum/len(recontruction_x)
            sum = 0
        elif l <= len(hidden_encoder) :
            for i in range(0,len(hidden_encoder)):
                hidden = hidden_encoder[i]
                for j in range(0,len(hidden_encoder[i])):
                    sum += hidden[j]
                sparsity[l] = sum/len(hidden)
                sum = 0
        else:
            for i in range(0,len(hidden_decoder)):
                hidden = hidden_decoder[i]
                for j in range(0, len(hidden_decoder[i])):
                    sum += hidden[j]
                sparsity[l] = sum/len(hidden)
                sum = 0
    return sparsity

def overall_cost_function(error, weight_decay, control_sparm, KL):
    sum = 0
    for i in range(0,len(error)):
        sum += error[i]

    mse = (sum/len(error))
    sparsity_penalty = control_sparm * KL
    ocf = mse + weight_decay + sparsity_penalty

    return mse, sparsity_penalty, ocf

def rmse(mse):
    return math.sqrt(mse)

def KullbackLeibler(sparsity, s_param):

    sum = 0
    for j in range(0,len(sparsity)):
        sum += (s_param * math.log(s_param/sparsity[j]) ) + ( (1-s_param)* math.log((1-s_param)/(1-sparsity[j])))
    return sum


"""
=====================================================================================================
                                    DATA JOINER
=====================================================================================================
"""

def joint_matrix1D(results):
    final_results = []
    for tmp in results:
        for _i in tmp:
            final_results.append(_i)
    return final_results

"""
=====================================================================================================
                                    PARALLEL COMPUTATION
=====================================================================================================
"""

def parallel_network_encoder(weights,bias,input,h_enc,e_size, COMM, RANK, SIZE):

    for l in range(0, e_size):
        if l == 0 :
            if RANK == 0 :
                Input_1D    = input
                Bias_1D     = bias[l]
                Weight_2D   = weights[l]
                Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)
            else:
                Input_1D    = None
                Bias_1D     = None
                Weight_2D   = None

            Weight_2D   = COMM.scatter(Weight_2D, root=0)
            Bias_1D     = COMM.scatter(Bias_1D, root=0)
            Input_1D    = COMM.bcast(Input_1D, root=0)

            Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
            Network = COMM.gather(Network, root=0)
            if RANK == 0 :
                h_enc[l] = joint_matrix1D(Network)


        else :
            index = l - 1
            if RANK == 0 :
                Input_1D    = h_enc[index]
                Bias_1D     = bias[l]
                Weight_2D   = weights[l]
                Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)
            else:
                Input_1D    = None
                Bias_1D     = None
                Weight_2D   = None

            Weight_2D   = COMM.scatter(Weight_2D, root=0)
            Bias_1D     = COMM.scatter(Bias_1D, root=0)
            Input_1D    = COMM.bcast(Input_1D, root=0)

            Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
            Network = COMM.gather(Network, root=0)

            if RANK == 0 :
                h_enc[l] = joint_matrix1D(Network)

    return h_enc

def parallel_network_decoder(weights,bias,input,h_dec,d_size, COMM, RANK, SIZE):

    for l in range(0, d_size):
        if l == 0 :
            if RANK == 0 :
                Input_1D    = input
                Bias_1D     = bias[l+d_size]
                Weight_2D   = weights[l]
                Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)
            else:
                Input_1D    = None
                Bias_1D     = None
                Weight_2D   = None

            Weight_2D   = COMM.scatter(Weight_2D, root=0)
            Bias_1D     = COMM.scatter(Bias_1D, root=0)
            Input_1D    = COMM.bcast(Input_1D, root=0)

            Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
            Network = COMM.gather(Network, root=0)
            if RANK == 0 :
                h_dec[l] = joint_matrix1D(Network)


        elif l != d_size - 1:
            index = l - 1
            if RANK == 0 :
                Input_1D    = h_dec[index]
                Bias_1D     = bias[l+d_size]
                Weight_2D   = weights[l]
                Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)
            else:
                Input_1D    = None
                Bias_1D     = None
                Weight_2D   = None

            Weight_2D   = COMM.scatter(Weight_2D, root=0)
            Bias_1D     = COMM.scatter(Bias_1D, root=0)
            Input_1D    = COMM.bcast(Input_1D, root=0)

            Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
            Network = COMM.gather(Network, root=0)

            if RANK == 0 :
                h_dec[l] = joint_matrix1D(Network)

    return h_dec

def parallel_network_recontruction(weights,bias,input,output,d_size, COMM, RANK, SIZE):

    for l in range(0, d_size):
        if  l == d_size - 1 :
            if RANK == 0 :
                Input_1D    = input
                Bias_1D     = bias[l+d_size]
                Weight_2D   = weights[l]
                Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)
            else:
                Input_1D    = None
                Bias_1D     = None
                Weight_2D   = None

            Weight_2D   = COMM.scatter(Weight_2D, root=0)
            Bias_1D     = COMM.scatter(Bias_1D, root=0)
            Input_1D    = COMM.bcast(Input_1D, root=0)

            Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
            Network = COMM.gather(Network, root=0)

            if RANK == 0 :
                output = joint_matrix1D(Network)
    return output

def parallel_cost_function(output, input, COMM, RANK, SIZE):



    if RANK == 0 :
        Output_1D   = arp.split_1DMatrix(output, SIZE)
        Input_1D    = arp.split_1DMatrix(input, SIZE)
        error  = None
    else:
        Output_1D   = None
        Input_1D    = None
        error  = None

    Output_1D   = COMM.scatter(Output_1D, root=0)
    Input_1D   = COMM.scatter(Input_1D, root=0)

    Error = compute_cost_function(Output_1D , Input_1D, COMM, RANK, SIZE)
    Error = COMM.gather(Error, root=0)

    if RANK == 0 :
        error = joint_matrix1D(Error)

    return error

def parallel_weight_decay(weights, wdp, COMM, RANK, SIZE):

    sum = 0.0
    for l in range(0,len(weights)):
        if RANK == 0 :
            Weight_2D = weights[l]
            Weight_2D = arp.split_2DRMatrix(Weight_2D, SIZE)
        else :
            Weight_2D = None

        Weight_2D   = COMM.scatter(Weight_2D, root=0)
        sum_elemet = inner_weight_decay(Weight_2D, COMM, RANK, SIZE)

        local_wd = COMM.reduce(sum_elemet, op=MPI.SUM, root=0)

        if RANK == 0 :
            sum += local_wd

    return ( sum * wdp ) / 2
