import numpy as np
import math

def confution_matrix(predict_class, actual_class, K):
    """
    # Deskripsi
    confution_matrix() merupakan metode untuk menghitung jumlah kelas
    yang akurat saat klasisifikasi dijalankan. jumlah yang dihutng yaitu
    nilai benar dan salah antara kelas yang di prediksi dan kelas actual

    # Parameter

    """

    confution_matrix = np.zeros((K,K))

    for i in range(0, len(predict_class)):
        if predict_class[i] == actual_class[i] :
            confution_matrix[actual_class[i]][predict_class[i]] +=1
        else :
            confution_matrix[predict_class[i]][actual_class[i]] +=1


    return confution_matrix

def caclulate_label(confution_matrix,K):

    TP = np.zeros((K,1))
    TN = np.zeros((K,1))
    FP = np.zeros((K,1))
    FN = np.zeros((K,1))

    for k in range(0,K):
        for i in range(0, len(confution_matrix)):
            for j in range(0, len(confution_matrix[i])):
                if k == i and k == j :
                    #true positive
                    TP[k] = confution_matrix[i][j]
                elif i == j and k != i and k != j :
                    TN[k] += confution_matrix[i][j]
                elif j == k and i != k :
                    FP[k] += confution_matrix[i][j]
                elif j != k and i == k :
                    FN[k] += confution_matrix[i][j]

    return TP, TN, FP, FN

def accuracy(TP, TN, FP, FN, K ):

    acc = np.zeros((K,1))
    for k in range(0,K):
        acc[k] = ((TP[k]+TN[k])/(TP[k] + TN[k] + FP[k]+FN[k])) * 100

    return acc

def precision(TP, FP, K):

    precision = np.zeros((K,1))
    for k in range(0,K):
        precision[k] = ((TP[k])/(TP[k] +  FP[k])) * 100

    return precision

def recall(TP, FN, K):

    recall = np.zeros((K,1))
    for k in range(0,K):
        recall[k] = ((TP[k])/(TP[k] +  FN[k])) * 100

    return recall

def specificity(TN, FP, K):

    specificity  = np.zeros((K,1))
    for k in range(0,K):
        specificity [k] = ((TN[k])/(FP[k] +  TN[k])) * 100

    return specificity


def f_score(TP, FP, FN, K):

    f_score  = np.zeros((K,1))
    for k in range(0,K):
        f_score [k] = ((2*TP[k])/(2*TP[k] +  FP[k] + FN[k])) * 100

    return f_score

def overal_accuracy(TP,K,predict_class):
    sum = 0
    for k in range(0,K):
        sum += TP[k]
    return (sum/len(predict_class)) *100

def f_measure(predict_class, actual_class, K):
    cm = confution_matrix(predict_class, actual_class, K )
    TP, TN, FP, FN = caclulate_label(cm, K)
    acc = accuracy(TP, TN, FP, FN, K )
    pre = precision(TP, FP, K)
    rc = recall(TP, FN, K)
    spc = specificity(TN, FP, K)
    fs = f_score(TP, FP, FN, K)
    all_acc = overal_accuracy(TP,K,predict_class)
    persen_string = "%"
    for i in range(0, K):
        print("========================================================================")
        print(("  F-Measure for Class \t : %d ")%(i))
        print("------------------------------------------------------------------------")
        print("  TP    \t | TN     \t | FP     \t | FN     \t |")
        print(("  %g     \t | %g     \t | %g     \t | %g     \t |")%(TP[i], TN[i], FP[i], FN[i]))
        print("------------------------------------------------------------------------")
        print("  acc   \t | recall     \t | precison   \t | f-score     \t |")
        print(("  %.2f %s \t | %.2f %s    \t | %.2f %s   \t | %.2f %s   \t |")%(acc[i],"%",rc[i],"%",pre[i],"%",fs[i],"%"))
        print("========================================================================\n")

    print("========================================================================")
    print(("  Overal accuracy : %.2f %s ")%(all_acc,"%"))
    print("========================================================================\n")

def f_measure2(predict_class, actual_class, K):
    cm = confution_matrix(predict_class, actual_class, K )
    TP, TN, FP, FN = caclulate_label(cm, K)
    acc = accuracy(TP, TN, FP, FN, K )
    pre = precision(TP, FP, K)
    rc = recall(TP, FN, K)
    spc = specificity(TN, FP, K)
    fs = f_score(TP, FP, FN, K)
    all_acc = overal_accuracy(TP,K,predict_class)
    persen_string = "%"
    for i in range(0, K):
        print("========================================================================")
        print(("  F-Measure for Class \t : %d ")%(i))
        print("------------------------------------------------------------------------")
        print("  TP    \t | TN     \t | FP     \t | FN     \t |")
        print(("  %g     \t | %g     \t | %g     \t | %g     \t |")%(TP[i], TN[i], FP[i], FN[i]))
        print("------------------------------------------------------------------------")
        print("  acc   \t | recall     \t | precison   \t | f-score     \t |")
        print(("  %.2f %s \t | %.2f %s    \t | %.2f %s   \t | %.2f %s   \t |")%(acc[i],"%",rc[i],"%",pre[i],"%",fs[i],"%"))
        print("========================================================================\n")

    print("========================================================================")
    print(("  Overal accuracy : %.2f %s ")%(all_acc,"%"))
    print("========================================================================\n")

    return all_acc
