"""
----------------------------------------------------------------------------------
 * MPI module
----------------------------------------------------------------------------------
"""
from mpi4py import MPI
#from src import arrs as asp
"""
----------------------------------------------------------------------------------
 * Neural network Module
----------------------------------------------------------------------------------
"""
import autoencoder as ae
import feedforward as ff
import softmax as sm
from build_network import NeuralNetworkArch

"""
----------------------------------------------------------------------------------
 * Library
----------------------------------------------------------------------------------
"""
import pandas as pd
import numpy as np
import math
import os
import time
import import_data as id

"""
----------------------------------------------------------------------------------
 * Global Variable for Deep Neural Network
----------------------------------------------------------------------------------
"""
#-------------------------------------------------------------------------------------
# * Hyperparameter for Neural network
# :wdp - weight decay parameter
# :s_param - sparsity parameter
# :learning_rate - learning rate
# :control_sparm - parameter for control sparsity
#-------------------------------------------------------------------------------------
wdp = 0.001
learning_rate = 0.05
s_param = 0.01
control_sparm = 0.001
noc = 4

#-------------------------------------------------------------------------------------
# * Parameter for Neural Network
# :x_size - number m example for input feature
# :h_size - number of hidden layer autoencoder neuron per layer
# :encoder_size - number matrix of encoder [Hidden layer] number neuron per layer
# :decoder_size - number matrix of decoder [Hidden layer] number neuron per layer
# :hl_total - number of hidden layer
# :y_size - number of output layer neuron ; m output feature equal m input feature
#-------------------------------------------------------------------------------------
x_size = 54675
h_size = [500, 400, 300, 400, 500]
encoder_size = [500, 400, 300]
decoder_size = [300, 400, 500]
hl_total = 3
y_size = x_size
trainning_time = 0

#-------------------------------------------------------------------------------------
# * Parameter for Neural Network Testing Stage
# :x_test - number m example for input feature
# :h_test - number of hidden layer autoencoder neuron per layer
# :ht_total - number of hidden layer
# :y_test - number of output layer neuron ; m output feature equal m input feature
#-------------------------------------------------------------------------------------
x_test = 54675
h_test = [500, 400, 300]
ht_total = len(h_test)
y_test = 300
test_time = 0


def Initial_MPICOMM():
    COMM = MPI.COMM_WORLD
    return COMM

def Initial_MPIRANK(COMM):
    RANK = COMM.Get_rank()
    return RANK

def Initial_MPISIZE(COMM):
    SIZE = COMM.Get_size()
    return SIZE

if __name__ == '__main__' :

    #---------------------------------------------------------------------------
    # MPI Initial for default communicator
    #---------------------------------------------------------------------------
    COMM = Initial_MPICOMM()
    RANK = Initial_MPIRANK(COMM)
    SIZE = Initial_MPISIZE(COMM)

    print ("Build Network .....")
    dnn = NeuralNetworkArch(x_size, h_size, hl_total, y_size, encoder_size, decoder_size)
    weights = dnn.set_weight()
    bias = dnn.set_bias()

    #---------------------------------------------------------------------------
    # Trainning Stage
    #---------------------------------------------------------------------------

    train_path = id.load_dataset("scenario/train_1.txt")
    weight_classifier = sm.set_weight(noc, 300)
    bias_classifier = sm.set_bias(noc)
    output_target = id.load_predict_table("scenario/train_1.txt")


    #-------------------------------------------------------------------------------------
    # * Variable for Container
    # :input - matrix for variable input
    # :hidden_encoder - matrix for stage of encoder
    # :hidden_decoder - matrix for stage of decoder
    # :recontruction_x - matrix of recontruction input x
    # :sparsity - matrix for sparsity value container
    # :path - list of dataset path
    #-------------------------------------------------------------------------------------
    input = []
    hidden_encoder = []
    hidden_decoder = []
    recontruction_x = []
    sparsity = []
    predict_class = []
    xent = []
    trainning_time = 0

    for i in range(0,len(train_path)):

        if RANK == 0 :
            #-------------------------------------------------------------------------------------
            # Varible for output of hidden layer autoencoder
            #-------------------------------------------------------------------------------------
            h_enc = dnn.set_ench()
            h_dec = dnn.set_dech()
            #-------------------------------------------------------------------------------------
            # Varible for derivative of backpropagation
            #-------------------------------------------------------------------------------------
            _dW = dnn.set_derivative_weight()
            _dB = dnn.set_derivatives_bias()

        else:
            h_enc = None
            h_dec = None
            _dW = None
            _dB = None
        h_enc = COMM.bcast(h_enc, root=0)
        h_dec = COMM.bcast(h_dec, root=0)
        _dW = COMM.bcast(_dW, root=0)
        _dB = COMM.bcast(_dB, root=0)
        

        #-------------------------------------------------------------------------------------
        # Copy data from file txt
        #-------------------------------------------------------------------------------------
        print ("----------------------------------------------------------------------------")
        print (" Step 1. Get dataset for input feature ... ...")
        input.append(id.copy_data_matrix(id.load_data(train_path[i])))

        #-------------------------------------------------------------------------------------
        # this stage is the stage for forward pass process using autoencoder method
        # ff.train_forward is module for compute forward pass
        #-------------------------------------------------------------------------------------
        print (" Step 2. Forward pass ... ...")
        #encoder  = ff.train_forward(dnn, weights, bias, input[i], decoder_size, encoder_size, h_enc, h_dec, wdp, h_size, s_param, control_sparm, COMM, RANK, SIZE)
        encoder =  ff.parallel_encoding(input[i], weights, h_enc  , bias,   encoder_size, COMM, RANK, SIZE)
        print(encoder)
