import pandas as pd
import numpy as np
#import arrs as asp
import math


def sigmoid_function(z):
    """
    fungsi sigmoid logistik 1 / 1 + exp(-x)
    :z merupakan kalkulasi network antara w,b dengan formulasi
    sigma W.x + b ; dimana x adalah nilai input dari input layer
    """
    return (1/(1+ math.exp(-z)))

def sigmoid_derivative(activation):
    """
    fungsi derivative dari fungsi logistik sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return activation * (1-activation)

"""
================================================================================
            SPLIT MODULE
================================================================================
"""
def split_1DMatrix(data, size):
    n = len(data);mod = n % size;bd = n - mod;slice = int(bd/size)
    send_arr = [];start = 0;end = 0

    for i in range(size):
        end = start + slice
        if mod != 0 :
            mod -=1
            end +=1
        send_arr.append(data[start:end])
        start = end
    return send_arr

def split_2DRMatrix(data, size): # Slice row
    n = len(data);mod = n % size;bd = n - mod;slice = int(bd/size)
    send_arr = [];start = 0;end = 0

    for i in range(size):
        end = start + slice
        if mod != 0 :
            mod -=1
            end +=1
        send_arr.append(data[start:end][:])
        start = end
    return send_arr

"""
================================================================================
            PARALLEL METHOD
================================================================================
"""

def summing_network(Weight, Input, Bias, N_Input):

    sum = 0
    for j in range(0, N_Input + 1):
        if j == N_Input :
            sum += Bias
        else:
            sum += Weight[j] * Input[j]
    return sum

def activation_calculation(Weight, Input, Bias, N_Input, N_Hidden):
    """
    #   Bagi total n_hidden berdasarkan jumlah matrix yang sudah di slice
    #   setelah itu running looping sebanyak matrix yang telah di pecah
    #   ambil jumlah N dengan len(data)
    """

    Network = np.zeros((N_Hidden))

    for i in range(0, N_Hidden):
        Network[i] = sigmoid_function( summing_network(Weight[i], Input, Bias[i], N_Input) )
    return Network

def joint_matrix1D(results, RANK):

    if RANK == 0 :
        final_results = []
        for tmp in results:
            for _i in tmp:
                final_results.append(_i)
        return final_results

def parallel_network(Weight, Input, Bias, N_Hidden, N_Input, COMM, RANK, SIZE):

    #================================================================================
    # Prepare to Send Data to other processor using communicator
    # Data dari Weight dibagi menjadi sebanyak SIZE
    # data dari input akan di kirim ke seluruh prosesor menggunakan broadcast
    # data dari bias akan dibagi menjadi sebnanyak SIZE
    #================================================================================
    if RANK == 0 :
        Input_1D    = Input
        Bias_1D     = Bias
        Weight_2D   = Weight
        Weight_2D   = split_2DRMatrix(Weight_2D, SIZE)
        Bias_1D     = split_1DMatrix(Bias_1D, SIZE)
    else:
        Input_1D    = None
        Bias_1D     = None
        Weight_2D   = None

    #================================================================================
    # Send Data to other processor using communicator
    # :Weight_2D can be Send using MPI.COMM_WORLD.scatter
    #================================================================================
    Weight_2D   = COMM.scatter(Weight_2D, root=0)
    Bias_1D     = COMM.scatter(Bias_1D, root=0)
    Input_1D    = COMM.bcast(Input_1D, root=0)

    #================================================================================
    # Parallel Computation
    #================================================================================
    Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D))
    COMM.Barrier()

    Network = COMM.gather(Network, root=0)

    return joint_matrix1D(Network,RANK)
