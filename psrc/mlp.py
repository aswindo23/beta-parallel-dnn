import pandas as pd
import numpy as np
import math
import arrs as arp
import sys
from mpi4py import MPI

def sigmoid_function(z):
    """
    fungsi sigmoid logistik 1 / 1 + exp(-x)
    :z merupakan kalkulasi network antara w,b dengan formulasi
    sigma W.x + b ; dimana x adalah nilai input dari input layer
    """
    return (1/(1+ math.exp(-z)))

def sigmoid_derivative(activation):
    """
    fungsi derivative dari fungsi logistik sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return activation * (1-activation)

def cost_function(output,y):
    """
    cost function merupakan fungsi biaya yang mengkalkulasikan
    antara output target dengan output aktual dimana output actual adalah y
    dan output target dalah fungsi hypothesis
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    :y merupakan output aktual
    """
    return 0.5 * math.pow(abs(output - y ) , 2)

def summing_network(Weight, Input, Bias, N_Input):

    sum = 0.0
    for j in range(0, N_Input + 1):
        if j == N_Input :
            sum += Bias
        else:
            sum += Weight[j] * Input[j]
    return sum

def activation_calculation(Weight, Input, Bias, N_Input, N_Hidden, COMM, RANK, SIZE):
    """
    #   Bagi total n_hidden berdasarkan jumlah matrix yang sudah di slice
    #   setelah itu running looping sebanyak matrix yang telah di pecah
    #   ambil jumlah N dengan len(data)
    """

    Network = np.zeros((N_Hidden))
    for r in range(SIZE):
        if RANK == r :
            for i in range(0, N_Hidden):
                Network[i] = sigmoid_function( summing_network(Weight[i], Input, Bias[i], N_Input) )
            COMM.Barrier()
    return Network

def compute_cost_function(output, y_target):
    sum = 0
    error_cf = []
    for i in range(0,len(output)):
         error_cf.append(cost_function(output[i], y_target))

    return error_cf

def inner_weight_decay(weights, COMM, RANK, SIZE):

    sum = 0.0
    for r in range(SIZE):
        if RANK == r :
            for i in range(0, len(weights[0])):
                for j in range(0, len(weights)):
                    sum += math.pow(weights[j][i], 2)
            COMM.Barrier()
    return sum

def overall_cost_function(m_cost_function, weight_decay):
    sum = 0
    for i in range(0,len(m_cost_function)):
        sum += m_cost_function[i]
    return (sum/len(m_cost_function)) + (weight_decay)

"""
=====================================================================================================
                                    DATA JOINER
=====================================================================================================
"""

def joint_matrix1D(results):
    final_results = []
    for tmp in results:
        for _i in tmp:
            final_results.append(_i)
    return final_results

"""
=====================================================================================================
                                    PARALLEL COMPUTATION
=====================================================================================================
"""

def parallel_weight_decay(weights, wdp, COMM, RANK, SIZE):

    sum = 0.0
    for l in range(0,len(weights)):
        if RANK == 0 :
            Weight_2D = weights[l]
            Weight_2D = arp.split_2DRMatrix(Weight_2D, SIZE)
        else :
            Weight_2D = None

        Weight_2D   = COMM.scatter(Weight_2D, root=0)
        sum_elemet = inner_weight_decay(Weight_2D, COMM, RANK, SIZE)

        local_wd = COMM.reduce(sum_elemet, op=MPI.SUM, root=0)

        if RANK == 0 :
            sum += local_wd

    return ( sum * wdp ) / 2

def parallel_forward_output(Input, Weight, Bias, Output,  hSize, COMM, RANK, SIZE):

    for l in range(0, len(hSize) + 1 ):
        if l == len(hSize) -1 :
            if RANK == 0 :

                Weight_2D   = Weight[l]
                Input_1D    = Input
                Bias_1D     = Bias[l]
                Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)
            else :
                Weight_2D   = None
                Input_1D    = None
                Bias_1D     = None

            Weight_2D   = COMM.scatter(Weight_2D, root=0)
            Bias_1D     = COMM.scatter(Bias_1D, root=0)
            Input_1D    = COMM.bcast(Input_1D, root=0)

            Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
            Network = COMM.gather(Network, root=0)

            if RANK == 0 :
                Output = joint_matrix1D(Network)

    return Output

def parallel_forward_hidden(Input, Weight, HiddenLayer, Bias,  hSize, COMM, RANK, SIZE):

    for l in range(0, len(hSize) + 1):
        if l != len(hSize) :
            if RANK == 0 :
                if l == 0 :
                    Input_1D    = Input
                    Weight_2D   = Weight[l]
                    Bias_1D     = Bias[l]
                    Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                    Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)

                elif l != len(hSize):
                    Input_1D    = HiddenLayer[l-1]
                    Weight_2D   = Weight[l]
                    Bias_1D     = Bias[l]
                    Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
                    Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)

            else:

                Input_1D    = None
                Weight_2D   = None
                Bias_1D     = None

            Weight_2D   = COMM.scatter(Weight_2D, root=0)
            Bias_1D     = COMM.scatter(Bias_1D, root=0)
            Input_1D    = COMM.bcast(Input_1D, root=0)

            Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
            Network = COMM.gather(Network, root=0)

            if RANK == 0 :
                HiddenLayer[l] = joint_matrix1D(Network)


    return HiddenLayer
