"""
----------------------------------------------------------------------------------
 * Library
----------------------------------------------------------------------------------
"""
import os


def load_data(file):
    f = open(file,"r", encoding="utf8")
    data = f.readlines()
    f.close()
    return data

def copy_data_matrix(data):

    data_txt = data
    data_mtx = []

    for line in data_txt:
        data_col = line.split('\n')
        tmp_value = data_col[0]
        data_mtx.append(float(tmp_value))

    return data_mtx

def load_all_data(file):

    path = []
    f = open(file, "r", encoding="utf8")
    dataset = f.readlines()
    for i in dataset:
        data_col = i.split('\n')
        path.append(data_col[0])
    f.close()

    all_path = []
    for line in range(0,len(path)):
        for root, dirs, files in os.walk(path[line]):
            for filename in files:
                if filename[0:5] == "_NORM" :
                    all_path.append(path[line] + filename)
    return all_path

def load_predict_table(file):
    predict_class = []
    f = open(file, "r", encoding="utf8")
    dataset = f.readlines()

    for i in dataset:
        data_col = i.split('\t')
        predict_class.append(int(data_col[1]))
    f.close()

    return predict_class


def load_dataset(file):
    data_train = []

    dataset = load_data(file)

    for i in dataset:
        data_col = i.split('\t')
        data_train.append(str(data_col[0]))

    return data_train
