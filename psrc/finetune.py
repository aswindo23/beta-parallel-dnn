import pandas as pd
import numpy as np
import math
import arrs as arp
import time

def sigmoid_derivative(activation):
    """
    fungsi derivative dari fungsi logistik sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return activation * (1-activation)

def derivative_error(output, target):
    return -(target - output) * sigmoid_derivative(output)

def derivative_hidden_layer(dE, weight):
    return weight * dE

def derivative_sparsity_contrain(sparam, sparsity, c_sparam):

    return c_sparam * ((-sparam / sparsity) + ((1-sparam) / (1-sparsity)))


def compute_dE(output, target, COMM, RANK , SIZE):

    itr_size = len(output)
    DE = np.zeros((itr_size,1))
    for r in range(SIZE):
        if RANK == r :
            for i in range(0,itr_size):
                DE[i] = derivative_error(output[i],target[i])
            COMM.Barrier()
    return DE

def inner_compute_dH(delta,weights,i):
    sum = 0.0

    for j in range(0,len(weights[0])):
        sum += derivative_hidden_layer(delta[j], weights[i][j])
    return sum

def compute_dH(weights, delta, hidden_layer , sparam, sparsity, c_sparam,  COMM, RANK, SIZE):

    sum = 0.0
    _tmpdH = np.zeros((len(weights),1))

    for r in range(SIZE):
        if RANK == r :
            for i in range(0,len(weights)):
                sum = inner_compute_dH(delta,weights,i)
                _tmpdH[i] = (sum * derivative_sparsity_contrain(sparam, sparsity, c_sparam ) )  *  sigmoid_derivative(hidden_layer[i])
            COMM.Barrier()
    return _tmpdH

def inner_dev_weights(weightsDev, wdp, weights, delta, input ):

    for i in range(0, len(weightsDev[0])): #ntra di tuker ngikutin dulu
        m = len(weightsDev[0])
        #print("jumlah M %s"%(m))
        for j in range(0, len(weightsDev)):
            #print("LEN weightsDev %s"%(len(weightsDev)))
            #print("len() delta %s" %(len(delta)))
            #print("len() Input %s" %(len(input)))
            #print("len() weight %s" %(len(weights)))
            #print("len() weight[0] %s" %(len(weights[0])))
            #print("weight %s" %(weights))
            weightsDev[j][i] = (( delta[j] * input[i] ) / m ) + ( wdp * weights[j][i] )

    return weightsDev

def inner_dev_bias(biasDev, delta):

    for i in range(0, len(biasDev[0])):
        m = len(biasDev[0])
        for j in range(0, len(biasDev)):
            biasDev[j][i] = (( delta[j]  ) / m )

    return biasDev

def inner_update_weight(weightsDev, weights, learning_rate):

    for i in range(0, len(weights)):
        for j in range(0, len(weights[0])):
            weights[i][j] = weights[i][j] - (learning_rate * weightsDev[i][j])

    return weights

def inner_update_bias(biasDev, bias, learning_rate):

    for i in range(0, len(bias)):
        for j in range(0, len(bias[0])):
            bias[i][j] = bias[i][j] - (learning_rate * biasDev[i][j])

    return bias



"""
=====================================================================================================
                                    PARALLEL COMPUTATION
=====================================================================================================
"""

def parallel_update_bias(biasDev, bias, learning_rate, COMM, RANK, SIZE):

    for l in range(0, len(bias)):
        if RANK == 0 :
            BiasDev_2D  = biasDev[l]
            Bias_2D     = bias[l]
            BiasDev_2D   =   arp.split_2DRMatrix(BiasDev_2D, SIZE)
            Bias_2D       = arp.split_2DRMatrix(Bias_2D, SIZE)
            LearningRate_1V = learning_rate
        else:
            BiasDev_2D      = None
            Bias_2D         = None
            LearningRate_1V = None

        BiasDev_2D      = COMM.scatter(BiasDev_2D, root=0)
        Bias_2D         = COMM.scatter(Bias_2D, root=0)
        LearningRate_1V = COMM.bcast(LearningRate_1V, root=0)

        _tmpBias = inner_update_bias(BiasDev_2D, Bias_2D, LearningRate_1V)
        _tmpBias = COMM.gather(_tmpBias, root=0)

        if RANK == 0 :
            bias[l] = arp.joint_matrix1DNP(_tmpBias)

    return bias



def parallel_update_weight(weightsDev, weights, learning_rate, COMM, RANK, SIZE):

    for l in range(0, len(weights)):
        if RANK == 0:
            WeightNew       = weights
            WeightsDev_2D   = weightsDev[l]
            Weight_2D       = weights[l]
            WeightsDev_2D   = arp.split_2DRMatrix(WeightsDev_2D, SIZE)
            Weight_2D       = arp.split_2DRMatrix(Weight_2D, SIZE)
            LearningRate_1V = learning_rate
        else:
            WeightNew       = None
            WeightsDev_2D   = None
            Weight_2D       = None
            LearningRate_1V = None

        WeightsDev_2D   = COMM.scatter(WeightsDev_2D, root=0)
        Weight_2D   = COMM.scatter(Weight_2D, root=0)
        LearningRate_1V   = COMM.bcast(LearningRate_1V, root=0)

        _tmpWeights = inner_update_weight(WeightsDev_2D, Weight_2D, LearningRate_1V )
        _tmpWeights = COMM.gather(_tmpWeights, root=0)

        if RANK == 0 :
            WeightNew[l] = arp.joint_matrix1DNP(_tmpWeights)

    return WeightNew

def parallel_compute_bias_D(encDev, biasDev, decDev, errorDev, COMM, RANK, SIZE ):

    for l in range(0,len(biasDev)):
        if RANK == 0 :

            if l < len(encDev) :
                _tmpDev = encDev[len(encDev) - (l+1)]
            else:
                _tmpDev = decDev[len(encDev) - (l-1)]

            if l == 0 :
                Delta_1D    = _tmpDev
                Delta_1D    = arp.split_1DMatrix(Delta_1D, SIZE)
            elif l != len(biasDev) -1 :
                Delta_1D    = _tmpDev
                Delta_1D    = arp.split_1DMatrix(Delta_1D, SIZE)
            elif l == len(biasDev) -1 :
                Delta_1D    = errorDev
                Delta_1D    = arp.split_1DMatrix(Delta_1D, SIZE)

            BiasDev_2D  = biasDev[l]
            BiasDev_2D   = arp.split_2DRMatrix(BiasDev_2D, SIZE)

        else:
            BiasDev_2D  = None
            Delta_1D    = None


        BiasDev_2D = COMM.scatter(BiasDev_2D, root=0)
        Delta_1D = COMM.scatter(Delta_1D, root=0)

        _tmpBiasDev_2D = inner_dev_bias(BiasDev_2D,Delta_1D)
        _tmpBiasDev_2D = COMM.gather(_tmpBiasDev_2D, root=0)

        if RANK == 0 :
            biasDev[l] = arp.joint_matrix1D(_tmpBiasDev_2D)

    return biasDev

def parallel_compute_weight_D(encDev, weightsDev, decDev, input, encHid, wdp, weights , COMM, RANK, SIZE ):

    for l in range(0, len(weightsDev)):
        if RANK == 0 :

            if l == 0 :
                #print("LEN WD %s "%(len(weightsDev)))
                #print("L + 1 % s"%(l+1))
                Delta_1D        = encDev[ len(weightsDev) - (l+1) ]
                Input_1D        = input
                Delta_1D   = arp.split_1DMatrix(Delta_1D, SIZE) # scatter
            elif  l != len(weightsDev) -1 :
                Delta_1D        = encDev[ len(weightsDev) - (l+1) ]
                Input_1D       = encHid[l-1]
                Delta_1D   = arp.split_1DMatrix(Delta_1D, SIZE) # scatter

            elif  l == len(weightsDev) -1 :
                Input_1D       = encHid[l-1]
                Delta_1D       = decDev
                Delta_1D   = arp.split_1DMatrix(Delta_1D, SIZE) # scatter

            WeightsDev_2D   = weightsDev[l]
            Weight_2D       = weights[l]
            WDP_1V          = wdp
            Weight_2D       = arp.split_2DRMatrix(Weight_2D, SIZE) # scatter
            WeightsDev_2D   = arp.split_2DRMatrix(WeightsDev_2D, SIZE) # scatter

            #Delta_1D        = encDev[ len(weightsDev) - (l+1) ] # digunakan ketika if l == 0 : dan elif l != len(_dW) -1 :
            #EncHid_1D       = encHid[l-1] # digunakan ketika elif l != len(_dW) -1 : dan elif l == len(_dW) -1 :
            #Input_1D        = input # digunakan ketika if l == 0 :
            #DecDev_1D       = decDev # digunakan ketika elif l == len(_dW) -1 :
        else:
            WeightsDev_2D   = None
            Delta_1D        = None
            Input_1D        = None
            Weight_2D       = None
            WDP_1V          = None

        Delta_1D        = COMM.scatter(Delta_1D, root=0)
        Weight_2D       = COMM.scatter(Weight_2D, root=0)
        WeightsDev_2D   = COMM.scatter(WeightsDev_2D, root=0)
        WDP_1V          = COMM.bcast(WDP_1V, root=0)
        Input_1D        = COMM.bcast(Input_1D, root=0)

        _tmpWeightsDev_2D  = inner_dev_weights(WeightsDev_2D, WDP_1V, Weight_2D, Delta_1D, Input_1D )
        _tmpWeightsDev_2D = COMM.gather(_tmpWeightsDev_2D, root=0)

        if RANK == 0 :
            weightsDev[l] = arp.joint_matrix1D(_tmpWeightsDev_2D)

    return weightsDev

def parallel_compute_DE(output, target, COMM, RANK, SIZE):

    if RANK == 0 :
        Output_1D   = output
        Target_1D   = target
        Output_1D   = arp.split_1DMatrix(Output_1D, SIZE)
        Target_1D   = arp.split_1DMatrix(Target_1D, SIZE)
        dE = np.zeros((len(output),1))

    else :
        Output_1D   = None
        Target_1D   = None
        dE = None

    Output_1D     = COMM.scatter(Output_1D, root=0)
    Target_1D     = COMM.scatter(Target_1D, root=0)

    dE_element = compute_dE(Output_1D,Target_1D, COMM, RANK, SIZE)
    dE_element = COMM.gather(dE_element, root=0)

    if RANK == 0 :
        dE = arp.joint_matrix1DNP(dE_element)

    return dE

def parallel_compute_DECDH(weights, delta, hidden_layer , sparam, sparsity, c_sparam,  COMM, RANK, SIZE):

    derivative_dec = []
    for l in range(len(weights),1,-1):

        if RANK == 0 :

            if l == len(weights) :
                Delta_1D   = delta
            elif l != len(weights) :
                Delta_1D = dH


            Sparsity    = sparsity[l+1]
            Weight_2D   = weights[l-1].T
            Hidden_1D   = hidden_layer[l-2]
            Hidden_1D   = arp.split_1DMatrix(Hidden_1D, SIZE)
            Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)



        else :
            Delta_1D   = None
            Weight_2D   = None
            Hidden_1D   = None
            Sparsity = None

        Delta_1D     = COMM.bcast(Delta_1D, root=0)
        Hidden_1D     = COMM.scatter(Hidden_1D, root=0)
        Weight_2D     = COMM.scatter(Weight_2D, root=0)
        Sparsity     = COMM.bcast(Sparsity, root=0)

        tmpdH = compute_dH(Weight_2D, Delta_1D, Hidden_1D , sparam, Sparsity, c_sparam,  COMM, RANK, SIZE)
        tmpdH = COMM.gather(tmpdH, root=0)

        if RANK == 0 :
            dH = arp.joint_matrix1DNP(tmpdH)
            derivative_dec.append(dH)

    derivative_dec     = COMM.bcast(derivative_dec, root=0)
    return derivative_dec

def parallel_compute_ENCDH(weights, wt ,delta, hidden_layer , sparam, sparsity, c_sparam,COMM, RANK, SIZE):

    derivative_enc = []
    for l in range(len(weights),0,-1):

        if RANK == 0 :

            if l == len(weights) :
                Delta_1D    = delta
                Weight_2D   = wt[0].T
            elif l != len(weights) :
                Delta_1D    = dH
                Weight_2D   =weights[l].T


            Sparsity    = sparsity[l-1]

            Hidden_1D   = hidden_layer[l-1]
            Hidden_1D   = arp.split_1DMatrix(Hidden_1D, SIZE)
            Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)



        else :
            Delta_1D   = None
            Weight_2D   = None
            Hidden_1D   = None
            Sparsity = None

        Delta_1D     = COMM.bcast(Delta_1D, root=0)
        Hidden_1D     = COMM.scatter(Hidden_1D, root=0)
        Weight_2D     = COMM.scatter(Weight_2D, root=0)
        Sparsity     = COMM.bcast(Sparsity, root=0)

        tmpdH = compute_dH(Weight_2D, Delta_1D, Hidden_1D , sparam, Sparsity, c_sparam,  COMM, RANK, SIZE)
        tmpdH = COMM.gather(tmpdH, root=0)

        if RANK == 0 :
            dH = arp.joint_matrix1D(tmpdH)
            derivative_enc.append(dH)

    derivative_enc     = COMM.bcast(derivative_enc, root=0)
    return derivative_enc
