"""
----------------------------------------------------------------------------------
 * MPI module
----------------------------------------------------------------------------------
"""
from mpi4py import MPI
#from src import arrs as asp
"""
----------------------------------------------------------------------------------
 * Neural network Module
----------------------------------------------------------------------------------
"""
import autoencoder2 as ae
import feedforward as ff
import softmax as sm
import f_measure as fm
import finetune as ft
import mlp
from build_network import NeuralNetworkArch
from test_network import NeuroTest
"""
----------------------------------------------------------------------------------
 * Library
----------------------------------------------------------------------------------
"""
import pandas as pd
import numpy as np
import math
import os
import time
import import_data as id
import sys
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
SIZE = COMM.Get_size()


"""
----------------------------------------------------------------------------------
 * Global Variable for Deep Neural Network
----------------------------------------------------------------------------------
"""
#-------------------------------------------------------------------------------------
# * Hyperparameter for Neural network
# :wdp - weight decay parameter
# :s_param - sparsity parameter
# :learning_rate - learning rate
# :control_sparm - parameter for control sparsity
#-------------------------------------------------------------------------------------
wdp = 0.001
learning_rate = 0.05
s_param = 0.01
control_sparm = 0.001
noc = 8

#-------------------------------------------------------------------------------------
# * Parameter for Neural Network
# :x_size - number m example for input feature
# :h_size - number of hidden layer autoencoder neuron per layer
# :encoder_size - number matrix of encoder [Hidden layer] number neuron per layer
# :decoder_size - number matrix of decoder [Hidden layer] number neuron per layer
# :hl_total - number of hidden layer
# :y_size - number of output layer neuron ; m output feature equal m input feature
#-------------------------------------------------------------------------------------
x_size = 54675
h_size = [500, 400, 300, 400, 500]
encoder_size = [500, 400, 300]
decoder_size = [300, 400, 500]
hl_total = 3
y_size = x_size
trainning_time = 0

#-------------------------------------------------------------------------------------
# * Parameter for Neural Network Testing Stage
# :x_test - number m example for input feature
# :h_test - number of hidden layer autoencoder neuron per layer
# :ht_total - number of hidden layer
# :y_test - number of output layer neuron ; m output feature equal m input feature
#-------------------------------------------------------------------------------------
x_test = 54675
h_test = [500, 400, 300]
ht_total = len(h_test)
y_test = 300
test_time = 0




def sigmoid_derivative(activation):
    """
    fungsi derivative dari fungsi logistik sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return activation * (1-activation)




"""
================================================================================
            SPLIT MODULE
================================================================================
"""
def split_1DMatrix(data, size):
    n = len(data);mod = n % size;bd = n - mod;slice = int(bd/size)
    send_arr = [];start = 0;end = 0

    for i in range(size):
        end = start + slice
        if mod != 0 :
            mod -=1
            end +=1
        send_arr.append(data[start:end])
        start = end
    return send_arr

def split_2DRMatrix(data, size): # Slice row
    n = len(data);mod = n % size;bd = n - mod;slice = int(bd/size)
    send_arr = [];start = 0;end = 0

    for i in range(size):
        end = start + slice
        if mod != 0 :
            mod -=1
            end +=1
        send_arr.append(data[start:end][:])
        start = end
    return send_arr


"""
================================================================================
            SPLIT MODULE
================================================================================
"""



def train_forward(dnn, weights, bias, input, d_size, e_size, h_size):

    if RANK == 0 :
            #-------------------------------------------------------------------------------------
            # Varible for output of hidden layer autoencoder
            #-------------------------------------------------------------------------------------
        h_enc = dnn.set_ench()
        h_dec = dnn.set_dech()
        output = dnn.set_output()
        sparsity = np.zeros((1,1))
        KL = 0
        mse= 0; sparsity_penalty= 0; ocf = 0
        rmse = 0

    else:
        h_enc = None
        h_dec = None
        output = None
        sparsity = None
        KL = None
        mse= None; sparsity_penalty= None; ocf = None
        rmse = None

    h_enc = COMM.bcast(h_enc, root=0)
    h_dec = COMM.bcast(h_dec, root=0)
    output = COMM.bcast(output, root=0)

    #-------------------------------------------------------------------------------------
    #  Forward Pass
    #-------------------------------------------------------------------------------------
    COMM.Barrier()
    encoder = ae.parallel_network_encoder(weights,bias,input,h_enc,e_size, COMM, RANK, SIZE)
    COMM.Barrier()
    Wt = ae.weights_transpose(weights)
    COMM.Barrier()
    decoder = ae.parallel_network_decoder(Wt, bias, encoder[len(encoder)-1] , h_dec, d_size , COMM, RANK, SIZE)
    COMM.Barrier()
    _tmpRecon = ae.parallel_network_recontruction(Wt,bias,decoder[len(decoder)-1], output, d_size, COMM, RANK, SIZE )
    COMM.Barrier()
    error = ae.parallel_cost_function(_tmpRecon, input, COMM, RANK, SIZE)
    COMM.Barrier()
    wd = ae.parallel_weight_decay(weights, wdp, COMM, RANK, SIZE)
    #print("PRINT WD : %s"%(wd))
    COMM.Barrier()

    if RANK == 0 :
        sparsity = ae.sparsity_constrain( _tmpRecon, decoder, encoder, len(h_size) )
        KL = ae.KullbackLeibler(sparsity, s_param)
        mse, sparsity_penalty, ocf = ae.overall_cost_function(error, wd, control_sparm, KL)
        rmse = ae.rmse(mse)
    COMM.Barrier()

    return encoder,Wt, decoder, _tmpRecon, error, wd, sparsity, KL, mse, sparsity_penalty, ocf, rmse

def train_finetune(dnn, recontruction_input, input ,Wt, hidden_decoder,sparsity, weights, hidden_encoder, bias):
#-------------------------------------------------------------------------------------
#  Fine Tune
#-------------------------------------------------------------------------------------
    if RANK == 0 :
        #-------------------------------------------------------------------------------------
        # Varible for derivative of backpropagation
        #-------------------------------------------------------------------------------------
        weightsDev = dnn.set_derivative_weight()
        biasDev = dnn.set_derivatives_bias()
        derivative_dec = []

    else:
        weightsDev = None
        biasDev = None
        derivative_dec = None

    weightsDev = COMM.bcast(weightsDev, root=0)
    biasDev = COMM.bcast(biasDev, root=0)
    derivative_dec = COMM.bcast(derivative_dec, root=0)

    COMM.Barrier()
    derivative_err = ft.parallel_compute_DE(_tmpRecon, input, COMM, RANK, SIZE)
    derivative_dec = ft.parallel_compute_DECDH(Wt, derivative_err, hidden_decoder , s_param, sparsity, control_sparm  ,COMM, RANK, SIZE)
    derivative_enc = ft.parallel_compute_ENCDH(weights, Wt, derivative_dec[len(derivative_dec) - 1], hidden_encoder , s_param, sparsity, control_sparm,COMM, RANK, SIZE)
    WeightsDev_3D   = ft.parallel_compute_weight_D(derivative_enc, weightsDev, derivative_dec[len(derivative_dec) - 1], input, hidden_encoder, wdp, weights , COMM, RANK, SIZE )
    BiasDev_3D      = ft.parallel_compute_bias_D(derivative_enc, biasDev, derivative_dec, derivative_err, COMM, RANK, SIZE )
    Weights_Update  = ft.parallel_update_weight(WeightsDev_3D, weights, learning_rate, COMM, RANK, SIZE)
    Bias_Update   = ft.parallel_update_bias(biasDev, bias, learning_rate, COMM, RANK, SIZE)

    Weights_Update = COMM.bcast(Weights_Update, root=0)
    Bias_Update = COMM.bcast(Bias_Update, root=0)
    return Weights_Update, Bias_Update







if __name__ == '__main__' :

    dnn = NeuralNetworkArch(x_size, h_size, hl_total, y_size, encoder_size, decoder_size)
    weights = dnn.set_weight()
    bias = dnn.set_bias()

    input = []
    hidden_encoder = []
    hidden_decoder = []
    recontruction_x = []
    sparsity = []
    predict_class = []
    xent = []
    trainning_time = 0
    d_size = len(decoder_size)
    e_size = len(encoder_size)
    train_acc = 0
    test_acc = 0
    train_tracc = False
    test_teacc = False
    best_acc = False
    #---------------------------------------------------------------------------
    # Trainning Stage
    #---------------------------------------------------------------------------

    train_path = id.load_dataset("scenario/train_12.txt")
    weight_classifier = sm.set_weight(noc, 300)
    bias_classifier = sm.set_bias(noc)
    output_target = id.load_predict_table("scenario/train_12.txt")


    for i in range(0,len(train_path)):
        COMM.Barrier()
        t_start = MPI.Wtime()
        input.append(id.copy_data_matrix(id.load_data(train_path[i])))
        encoder,Wt, decoder, _tmpRecon, error, wd, s, KL, mse, sparsity_penalty, ocf, rmse = train_forward(dnn, weights, bias, input[i], d_size, e_size, h_size)
        hidden_encoder.append(encoder)
        hidden_decoder.append(decoder)
        recontruction_x.append(_tmpRecon)
        sparsity.append(s)

        weights, bias = train_finetune(dnn, recontruction_x, input[i], Wt, hidden_decoder[i],sparsity[i], weights, hidden_encoder[i], bias)


        encHid = hidden_encoder[i]
        InputClass = encHid[len(encHid) - 1]
        xent, pClass, NewWeightClass, NewBiasClass = sm.softmax_classifier(output_target[i], noc, weight_classifier, bias_classifier, InputClass, wdp, xent, learning_rate, COMM, RANK, SIZE  )

        predict_class.append(pClass)

        weight_classifier = NewWeightClass
        bias_classifier = NewBiasClass


        t_diff = MPI.Wtime() - t_start
        trainning_time += t_diff
        COMM.Barrier()

        if RANK == 0 :
            print ("============================================================================")
            print (" Summary ... ...")
            print (("\tInformation of Compute Data  - %d ... ... ...") % (i))
            print (("\tName \t\t\t : %s") % (train_path[i]))
            print (("\tMean Square error \t : %s ") % (mse) )
            print (("\tRoot MSE          \t : %s ") % (rmse) )
            print (("\tOverall Cost func \t : %s ") % (ocf) )
            print (("\tSparsity penalty  \t : %s ") % (sparsity_penalty) )
            print (("\tComputation time  \t : %f minute ") % ((t_diff)/60))
            print (("\tCurrently t time  \t : %f minute ") % (trainning_time/60))
            print (("\tWeight example 0   \t : %g")%(weights[0][0][0]))
            print (("\tWeight example 1   \t : %g")%(weights[1][1][1]))
            print ("----------------------------------------------------------------------------")
            print (" Summary ... ...")
            print (("\tInformasi Data ke %d ... ... ...") % (i))
            print (("\tName \t\t\t : %s") % (train_path[i]))
            print (("\toutput_target \t : %s ") % (output_target[i]) )
            print (("\tpredict_class \t : %s ") % (predict_class[i]) )
            print ("============================================================================")
            sys.stdout.flush()

    train_acc = fm.f_measure2(predict_class, output_target, noc)



    dnn_test = NeuroTest(x_test, h_test, ht_total, y_test )
    test_path = id.load_dataset("scenario/train_1.txt")
    output_target_test = id.load_predict_table("scenario/train_1.txt")

    input_test = []
    hidden_layer = []
    output_test = []
    predict_class_test = []
    test_time = 0
    h_enc = []
    xent = []

    for i in range(0,len(test_path)):
        COMM.Barrier()
        t_start = MPI.Wtime()
        hidden = dnn_test.set_hiddenlayer()
        input_test.append(id.copy_data_matrix(id.load_data(test_path[i])))
        hidden_layer.append(mlp.parallel_forward_hidden(input_test[i], weights, hidden, bias, h_test, COMM, RANK, SIZE ))
        hidden = hidden_layer[i]
        output_test.append(mlp.parallel_forward_output(hidden[len(h_test) - 1 ], weights, bias, dnn_test.set_output(),  h_test, COMM, RANK, SIZE))
        error = mlp.compute_cost_function(output_test[i], output_target_test[i])
        wd = mlp.parallel_weight_decay(weights, wdp, COMM, RANK, SIZE)
        mse = mlp.overall_cost_function(error, wd)


        h_enc.append(output_test[i])
        InputClassTest = h_enc[len(h_enc) - 1]
        xent, pClass, NewWeightClass, NewBiasClass = sm.softmax_classifier(output_target_test[i], noc, weight_classifier, bias_classifier, InputClassTest, wdp, xent, learning_rate, COMM, RANK, SIZE  )
        predict_class_test.append(pClass)

        t_diff = MPI.Wtime() - t_start
        test_time += t_diff
        COMM.Barrier()

        if RANK == 0 :
            print ("============================================================================")
            print (" Summary ... ...")
            print (("\tInformasi Data ke %d ... ... ...") % (i))
            print (("\tName \t\t\t : %s") % (test_path[i]))
            print (("\tMean Square error \t : %s ") % (mse) )
            print (("\tRoot MSE          \t : %s ") % (ae.rmse(mse)) )
            print (("\tComputation time  \t : %f minute ") % ((t_diff)/60))
            print (("\tCurrently t time  \t : %f minute ") % (test_time/60))
            print (("\tWeight example 0   \t : %g")%(weights[0][0][0]))
            print (("\tWeight example 1   \t : %g")%(weights[1][1][1]))
            print ("----------------------------------------------------------------------------")
            print (" Summary ... ...")
            print (("\tInformasi Data ke %d ... ... ...") % (i))
            print (("\tName \t\t\t : %s") % (train_path[i]))
            print (("\toutput_target \t : %s ") % (output_target_test[i]) )
            print (("\tpredict_class \t : %s ") % (predict_class_test[i]) )
            print ("============================================================================")
            sys.stdout.flush()
    test_acc = fm.f_measure2(predict_class, output_target, noc)
