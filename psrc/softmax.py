import pandas as pd
import numpy as np
import math
import arrs as arp
from mpi4py import MPI

def random_function( n1, n2):
    return  math.sqrt( 2 / (n1 + n2))

def set_weight(output_size, input_size):
    weight = np.random.rand(output_size , input_size) * random_function(output_size, input_size)
    return weight

def set_bias(output_size):
    bias = np.random.rand(output_size , 1)
    return bias

def sofmax_function(z):
    return math.exp(z)

def one_hot(y, k_class):

    y_target = np.zeros((k_class ))

    for j in range(0, k_class):
        if j == y :
            y_target[j] = 1
        else:
            y_target[j] = 0
    return y_target

def summing_network(Weight, Input, Bias, N_Input):

    sum = 0
    for j in range(0, N_Input + 1):
        if j == N_Input :
            sum += Bias
        else:
            sum += Weight[j] * Input[j]
    return sum

def activation_calculation(Weight, Input, Bias, N_Input, N_Hidden, COMM, RANK, SIZE):
    """
    #   Bagi total n_hidden berdasarkan jumlah matrix yang sudah di slice
    #   setelah itu running looping sebanyak matrix yang telah di pecah
    #   ambil jumlah N dengan len(data)
    """

    Network = np.zeros((N_Hidden))
    for r in range(SIZE):
        if RANK == r :
            for i in range(0, N_Hidden):
                Network[i] = sofmax_function( summing_network(Weight[i], Input, Bias[i], N_Input) )
            COMM.Barrier()
    return Network

def sum_hypothesis(output,COMM, RANK, SIZE):

    sum = 0
    for r in range(SIZE):
        if RANK == r :
            for i in range(0,len(output)):
                sum += output[i]

    return sum

def softmax_score(Output, SumSoftmax):

    score = np.zeros((len(Output)))

    for i in range(0,len(Output)):
        score[i] = Output[i]/SumSoftmax

    return score

def to_classlabel(score):
    max = 0
    predict_class = 0
    for i in range(0,len(score)):
        if max < score[i] :
            max = score[i]
            predict_class = i
    return predict_class

def cross_entropy(score, y_target, COMM, RANK, SIZE):

    sum = 0.0
    for r in range(SIZE):
        if RANK == r :
            for i in range(0,len(score)):
                sum += math.log(score[i]) * y_target[i]
    return - sum

def count_weight_decay(weights, wdp, COMM, RANK, SIZE):
    sum = 0

    for r in range(SIZE):
        if RANK == r :
            for i in range(0,len(weights)):
                for j in range(0,len(weights[i])):
                    sum += math.pow(weights[i][j], 2)

    return (wdp*sum)/2

def cost(cross_entropy, wd):
    sum = 0.0


    for i in range(0,len(cross_entropy)):
        sum += cross_entropy[i]
    return (sum/len(cross_entropy)) + (wd)

def cross_entropy_derivative(score, y_target):
    return (score - y_target)


def inner_weight_derivative(Score, yTarget, Input, Weight, wdp):

    weight_dev = np.zeros((len(Weight), len(Weight[0])))

    for i in range(0, len(Weight)):
        for j in range(0, len(Weight[i])):
            weight_dev[i][j] = ((cross_entropy_derivative(Score[i], yTarget[i]) * Input[i]) / len(Weight) + (wdp * Weight[i][j]))

    return weight_dev

def bias_derivative(Score, yTarget , nBias):

    bias_dev = np.zeros((len(Score), 1))

    for i in range(0, len(Score)):
            bias_dev[i] = (cross_entropy_derivative(Score[i],yTarget[i]) / nBias )

    return bias_dev

def update_weight(weight, dW, learning_rate):

    new_weight = weight

    for i in range(0,len(weight)):
        for j in range(0, len(weight[i])):
            new_weight[i][j] = weight[i][j] - learning_rate* dW[i][j]

    return new_weight

def update_bias(bias, dB, learning_rate):

    new_bias = bias

    for i in range(0,len(bias)):
            new_bias[i] = bias[i] - learning_rate* dB[i]

    return new_bias

def inner_update_weight(Weight, WeightDev, LearningRate):

    NewWeight = Weight

    for i in range(0, len(Weight)):
        for j in range(0, len(Weight[0])):
            NewWeight[i][j] = Weight[i][j] - (LearningRate * WeightDev[i][j] )

    return NewWeight

def inner_update_bias(Bias, biasDev, LearningRate):
    NewBias = Bias
    for i in range(0, len(Bias)):
        NewBias[i] = Bias[i]  - ( LearningRate * biasDev[i] )

    return NewBias

"""
=====================================================================================================
                                    PARALLEL COMPUTATION
=====================================================================================================
"""

def parallel_update_bias(Bias, biasDev, LearningRate, COMM, RANK, SIZE):

    NewBias = Bias

    if RANK == 0 :
        Bias_1D         = Bias
        BiasDev_1D      = biasDev
        LearningRate_1V = LearningRate
        Bias_1D         = arp.split_1DMatrix(Bias_1D, SIZE)
        BiasDev_1D      = arp.split_1DMatrix(BiasDev_1D, SIZE)

    else:
        Bias_1D     = None
        BiasDev_1D  = None
        LearningRate_1V = None

    Bias_1D     = COMM.scatter(Bias_1D, root=0)
    BiasDev_1D  = COMM.scatter(BiasDev_1D, root=0)
    LearningRate_1V  = COMM.bcast(LearningRate_1V, root=0)

    tmpBias = inner_update_bias(Bias_1D, BiasDev_1D, LearningRate_1V)
    tmpBias = COMM.gather(tmpBias, root=0)

    if RANK == 0 :
        NewBias = arp.joint_matrix1DNP(tmpBias)

    return NewBias



def parallel_update_weight(Weights, WeightDev, LearningRate, COMM, RANK, SIZE):

    NewWeight = Weights

    if RANK == 0 :
        Weight_2D       = Weights
        WeightsDev_2D   = WeightDev
        #print("len(Weights[0]) %s"%(len(Weight_2D[0])))
        #print("len(Weights) %s"%(len(Weight_2D)))

        Weight_2D       = arp.split_2DRMatrix(Weight_2D, SIZE)
        WeightsDev_2D   = arp.split_2DRMatrix(WeightsDev_2D, SIZE)
        LearningRate_1V = LearningRate

    else:
        Weight_2D       = None
        WeightsDev_2D   = None
        LearningRate_1V = None

    WeightsDev_2D = COMM.scatter(WeightsDev_2D, root=0)
    Weight_2D       = COMM.scatter(Weight_2D, root= 0)
    LearningRate_1V = COMM.bcast(LearningRate_1V, root=0)

    tmpWeight = inner_update_weight(Weight_2D, WeightsDev_2D, LearningRate_1V)
    tmpWeight = COMM.gather(tmpWeight, root= 0)

    print(tmpWeight)
    if RANK == 0 :
        NewWeight = arp.joint_matrix1DNP(tmpWeight)

    return NewWeight

def parallel_bias_dev(Score, yTarget, Bias, COMM, RANK, SIZE):

    Bias_Dev = np.zeros((len(Bias), 1))

    if RANK == 0 :
        Score_1D    = Score
        yTarget_1D  = yTarget
        nBias       = len(Bias)
        Score_1D    = arp.split_1DMatrix(Score_1D, SIZE)
        yTarget_1D  = arp.split_1DMatrix(yTarget, SIZE)
    else:
        Score_1D    = None
        yTarget_1D  = None
        nBias       = None

    Score_1D        = COMM.scatter(Score_1D, root=0)
    yTarget_1D      = COMM.scatter(yTarget_1D, root=0)
    nBias           = COMM.bcast(nBias, root=0)

    bias_dev =  bias_derivative(Score_1D, yTarget_1D , nBias)
    bias_dev = COMM.gather(bias_dev, root=0)

    if RANK == 0 :
        Bias_Dev = arp.joint_matrix1DNP2(bias_dev)

    return Bias_Dev

def parallel_weight_dev(Score, yTarget, Input, Weight, wdp, COMM, RANK, SIZE):

    Weight_Dev = np.zeros((len(Weight), len(Weight[0])))

    if RANK == 0 :
        Weight_2D   = Weight
        Score_1D    = Score
        Input_1D    = Input
        yTarget_1D  = yTarget
        WDP_1V      = wdp
        Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
        Score_1D    = arp.split_1DMatrix(Score_1D, SIZE)
        yTarget_1D  = arp.split_1DMatrix(yTarget_1D, SIZE)
        Input_1D    = arp.split_1DMatrix(Input_1D, SIZE)
    else:
        Weight_2D   = None
        Score_1D    = None
        Input_1D    = None
        yTarget_1D  = None
        WDP_1V      = None

    Weight_2D = COMM.scatter(Weight_2D, root=0)
    Score_1D = COMM.scatter(Score_1D, root=0)
    yTarget_1D = COMM.scatter(yTarget_1D, root=0)
    Input_1D = COMM.scatter(Input_1D, root=0)
    WDP_1V = COMM.bcast(WDP_1V, root=0)

    weight_dev = inner_weight_derivative(Score, yTarget, Input, Weight, wdp)
    weight_dev = COMM.gather(weight_dev, root=0)

    if RANK == 0 :
        Weight_Dev = arp.joint_matrix1DNP(weight_dev)

    return Weight_Dev


def parallel_weight_decay(Weights, wdp, COMM, RANK, SIZE):

    if RANK == 0 :
        WDP_1V     = wdp
        Weight_2D  = Weights
        Weight_2D  = arp.split_2DRMatrix(Weight_2D, SIZE)
    else:
        Weight_2D  = None
        WDP_1V     = None

    Weight_2D = COMM.scatter(Weight_2D, root=0)
    WDP_1V = COMM.bcast(WDP_1V, root=0)

    sum_element = count_weight_decay(Weight_2D, WDP_1V, COMM, RANK, SIZE)
    sum = COMM.reduce(sum_elemet, op=MPI.SUM, root=0)

    return sum


def parallel_cross_entropy(Score, yTarget, COMM, RANK, SIZE):

    if RANK == 0 :
        Score_1D    = Score
        yTarget_1D  = yTarget
        Score_1D  = arp.split_1DMatrix(Score_1D, SIZE)
        yTarget_1D  = arp.split_1DMatrix(yTarget_1D, SIZE)

    else:
        Score_1D    = None
        yTarget_1D  = None

    yTarget_1D  = COMM.scatter(yTarget_1D, root=-0)
    Score_1D    = COMM.scatter(Score_1D, root=-0)

    sum_elemet = cross_entropy(Score_1D, yTarget_1D, COMM, RANK, SIZE)
    sum = COMM.reduce(sum_elemet, op=MPI.SUM, root=0)

    return sum


def parallel_softmax_score(Output, SumSoftmax, COMM, RANK, SIZE):

    if RANK == 0 :
        SumSoftmax_1V   = SumSoftmax
        Output_1D       = Output
        Output_1D   =  arp.split_1DMatrix(Output_1D, SIZE)
    else:
        SumSoftmax_1V   = None
        Output_1D       = None

    Output_1D = COMM.scatter(Output_1D, root=0)
    SumSoftmax_1V = COMM.bcast(SumSoftmax_1V, root =0 )

    score = softmax_score(Output_1D, SumSoftmax_1V)
    score = COMM.gather(score)

    if RANK == 0 :
        score = arp.joint_matrix1D(score)

    score = COMM.bcast(score, root=0)
    return score

def parallel_hypothesis(Output,COMM, RANK, SIZE):

    sum = 0.0
    if RANK == 0 :
        Output_1D   = Output
        Output_1D   =  arp.split_1DMatrix(Output_1D, SIZE)
    else:
        Output_1D   = None

    Output_1D = COMM.scatter(Output_1D, root=0)

    sum_elemet  =  sum_hypothesis(Output_1D, COMM, RANK, SIZE)
    sum = COMM.reduce(sum_elemet, op=MPI.SUM, root=0)

    return sum

# Class => Classifier
def parallel_compute_network(weightClass, inputClass, biasClass, COMM, RANK, SIZE):

    if RANK == 0 :
        Input_1D    = inputClass #bcast
        Bias_1D     = biasClass
        Weight_2D   = weightClass
        Weight_2D   = arp.split_2DRMatrix(Weight_2D, SIZE)
        Bias_1D     = arp.split_1DMatrix(Bias_1D, SIZE)

    else :
        Input_1D    = None
        Bias_1D     = None
        Weight_2D   = None

    Weight_2D   = COMM.scatter(Weight_2D, root=0)
    Bias_1D     = COMM.scatter(Bias_1D, root=0)
    Input_1D    = COMM.bcast(Input_1D, root=0)

    Network = activation_calculation(Weight_2D, Input_1D, Bias_1D, len(Input_1D), len(Weight_2D), COMM, RANK, SIZE)
    Network = COMM.gather(Network, root=0)

    if RANK == 0 :
        Network = arp.joint_matrix1D(Network)

    return Network

def softmax_classifier(OutputTarget, noc, weightClass, biasClass , inputClass,wdp, xent, LearningRate,COMM, RANK, SIZE  ):

    true_label = one_hot(OutputTarget, noc)
    NetIn = parallel_compute_network(weightClass, inputClass, biasClass, COMM, RANK, SIZE)
    SumScore = parallel_hypothesis(NetIn, COMM, RANK, SIZE)
    score = parallel_softmax_score(NetIn, SumScore, COMM, RANK, SIZE)
    predict_class = to_classlabel(score)
    _tpmXent = parallel_cross_entropy(score, true_label, COMM, RANK, SIZE)
    xent.append(_tpmXent)
    xent = COMM.bcast(xent, root=0)
    wdClass = count_weight_decay(weightClass, wdp, COMM, RANK, SIZE)
    cost_value = cost(xent, wdClass)
    wClassDev = parallel_weight_dev(score, true_label, inputClass, weightClass, wdp, COMM, RANK, SIZE)

    bClassDev = parallel_bias_dev(score, true_label, biasClass, COMM, RANK, SIZE)

    NewWeightClass  =  update_weight(weightClass, wClassDev, LearningRate)
    NewBiasClass    =  update_bias(biasClass, bClassDev, LearningRate)
    return xent, predict_class, NewWeightClass, NewBiasClass
