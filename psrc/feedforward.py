import autoencoder as ae



#================================================================================
# Main Program
#================================================================================


def parallel_encoding(input, weights, hidden_layer, bias,  h_size, COMM, RANK, SIZE):

    if RANK == 0 :
        hidden_layer = hidden_layer
        input = input
        weights = weights
        bias = bias
        h_size = h_size
    else:
        hidden_layer = None
        input = None
        weights = None
        bias = None
        h_size = None


    for l in range(0, len(h_size)):
        if l == 0 :
            hidden_layer[l] = ae.parallel_network(weights[l], input, bias[l], h_size[l], len(input), COMM, RANK, SIZE)
            #print("masuk iterasi %s | rank %s" %(l,RANK))
        else :
            #print("masuk iterasi %s | rank %s" %(l,RANK))
            index = l - 1
            hidden_layer[l] = ae.parallel_network(weights[l], hidden_layer[index], bias[l], h_size[l], len(hidden_layer[index]), COMM, RANK, SIZE)
        
    return hidden_layer


def train_forward(dnn, weights, bias, input, decoder_size, encoder_size, h_enc, h_dec , wdp, h_size, s_param, control_sparm, COMM, RANK, SIZE):

    encoder =  parallel_encoding(input, weights, h_enc  , bias,   encoder_size, COMM, RANK, SIZE)
    #Wt = ae.weights_transpose(weights)
    #decoder = decoder_forward_pass(encoder[len(encoder)-1], Wt, h_dec  , bias,   decoder_size)
    #_tmpRecon, z_output = recontruction_input(decoder[len(decoder) - 1 ], Wt,  bias,  dnn.set_output(), decoder_size)
    #error = compute_cost_function(_tmpRecon, input )
    #wd = ae.count_weight_decay(weights,wdp)
    #sparsity = ae.sparsity_constrain( _tmpRecon, decoder, encoder, len(h_size) )
    #KL = ae.KullbackLeibler(sparsity, s_param)
    #mse, sparsity_penalty, ocf = ae.overall_cost_function(error, wd, control_sparm, KL)
    #rmse = ae.rmse(mse)
    #, decoder, _tmpRecon, error, wd, sparsity, KL, mse, sparsity_penalty, ocf, rmse, Wt
    return encoder
