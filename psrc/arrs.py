import numpy as np


def split_1DMatrix(data, size):
    n = len(data);mod = n % size;bd = n - mod;slice = int(bd/size)
    send_arr = [];start = 0;end = 0

    for i in range(size):
        end = start + slice
        if mod != 0 :
            mod -=1
            end +=1
        send_arr.append(data[start:end])
        start = end
    return send_arr

def split_2DRMatrix(data, size): # Slice row
    n = len(data);mod = n % size;bd = n - mod;slice = int(bd/size)
    send_arr = [];start = 0;end = 0

    for i in range(size):
        end = start + slice
        if mod != 0 :
            mod -=1
            end +=1
        send_arr.append(data[start:end][:])
        start = end
    return send_arr

def split_2DCMatrix(data, size): # Slice Colom
    data = data.T
    n = len(data[0])
    mod = n % size
    bd = n - mod
    slice = int(bd/size)

    send_arr = []
    start = 0
    end = 0

    for i in range(size):
        end = start + slice
        if mod != 0 :
            mod -=1
            end +=1

        send_arr.append(data[start:end][:])
        start = end

    return send_arr

def joint_matrix1D(results):
    final_results = []
    for tmp in results:
        for _i in tmp:
            final_results.append(_i)
    return final_results

def joint_matrix1DNP2(results):
    sum = 0

    for tmp in results:
        sum += len(tmp)

    final_results = np.zeros((sum,1))
    i = 0
    for tmp in results:
        for _i in tmp:
            final_results[i] = _i
            i+=1
    return final_results

def joint_matrix1DNP(results):
    sum = 0
    kolom = 0
    for tmp in results:
        sum += len(tmp)
        kolom =len(tmp[0])

    final_results = np.zeros((sum,kolom))
    i = 0
    for tmp in results:
        for _i in tmp:
            final_results[i] = _i
            i+=1
    return final_results


"""
if __name__ == '__main__' :

    size = 4
    my_arr = np.random.randint(5, size=(54675, 500))
    print(my_arr)
    my_arr = my_arr.T
    sendbuf = split_2DRMatrix(my_arr, size)
    #sendbuf = split_2DRMatrix(my_arr, size)
    #sendbuf = split_2DCMatrix(my_arr, size)

    #print(my_arr)
    #print("total array %s" % (len(my_arr)))
    #print("--------------")
    #my_arr = my_arr[0:2][:]
    #print(my_arr)
    print(sendbuf)
    el = sendbuf[0]
    print("total array %s" % (len(el[0])))
    print("total array %s" % (len(el)))
"""
